import nltk
import random
import math

from nltk.sentiment import SentimentIntensityAnalyzer
sia = SentimentIntensityAnalyzer()

POSITIVE_EMOJIS = ["✨", "❤️", "😍", "🥰"]
NEGATIVE_EMOJIS = ["👎", "😤", "😡", "🤬"]
THRESHOLD = 0.2

nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('vader_lexicon')


tweets = [
 "dear @verizonsupport your service is straight worst in dallas.. been with y'all over a decade and this is all time low for y'all. i'm talking no internet at all.",
 "@verizonsupport I sent you a dm",
 "thanks to michelle et al at @verizonsupport who helped push my no-show-phone problem along. Order canceled successfully, and I ordered this for pickup today at the Apple store in the mall."
 ]

def get_emojis(compound: float) -> str:
    if compound < THRESHOLD:
        return random.choice(NEGATIVE_EMOJIS)
    elif compound > THRESHOLD:
        return random.choice(POSITIVE_EMOJIS)
    return ""


def add_emojis_to_text(text: str) -> str:
    compound = sia.polarity_scores(text)["compound"]
    final_text = ""
    for i, c in enumerate(text):
        if c == ".":
            final_text += get_emojis(compound)
        elif c == " " and math.fabs(compound) > THRESHOLD:
            if random.random() < math.fabs(compound):
                final_text += f" {get_emojis(compound)} "
            else:
                final_text += " "
        else:
            final_text += c

    return final_text


if __name__ == '__main__':
    for tweet in tweets:
        print(add_emojis_to_text(tweet))
